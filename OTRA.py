import random
import time


def read_col_files(instance_file: str):
    graph = {}
    degree = {}
    """Read .col (DIMACS) file"""
    with open(instance_file, "r", encoding="utf8") as file:
        for line_ in file:
            line = line_.strip()
            if line.startswith("c"):
                continue
            if line.startswith("p"):
                _, _, nb_vertices_str, _ = line.split()
                nb_vertices = int(nb_vertices_str)
            elif line.startswith("e"):
                _, vertex1_str, vertex2_str = line.split()
                vertex1_ = int(vertex1_str) - 1
                vertex2_ = int(vertex2_str) - 1
                if vertex1_ == vertex2_:
                    continue
                if vertex1_ not in graph:
                    graph[vertex1_] = [vertex2_]
                    degree[vertex1_] = 1
                else:
                    graph[vertex1_].append(vertex2_)
                    degree[vertex1_] = degree[vertex1_] + 1
                if vertex2_ not in graph:
                    graph[vertex2_] = [vertex1_]
                    degree[vertex2_] = 1
                else:
                    graph[vertex2_].append(vertex1_)
                    degree[vertex2_] = degree[vertex2_] + 1
    for vertex in range(nb_vertices):
        if vertex not in graph:
            graph[vertex] = []
            degree[vertex] = 0
    return nb_vertices, graph, degree


def read_weights_file(weights_file: str, nb_vertices: int):
    """Read weights file and check the number of vertices"""
    if weights_file == "":
        return [1] * nb_vertices

    with open(weights_file, "r", encoding="utf8") as file:
        weights = list(map(int, file.readlines()))
    assert len(weights) == nb_vertices
    return weights


def get_next_vertex(vertices, edges, weights):
    max_vertex = random.choice(vertices)
    if max_vertex != -1:
        vertices.remove(max_vertex)
    return vertices, max_vertex


def generate_initial_population(population_size, population, edges, vertex_size, weights):
    used_colors = []
    for i in range(population_size):
        individual = {}
        num_colors = 1
        vertices = [i for i in range(vertex_size)]
        # Loop over the vertices and assign a random color that does not conflict with adjacent vertices
        while len(vertices) > 0:
            vertices, vertex = get_next_vertex(vertices, edges, weights)
            available_colors = set(range(1, num_colors + 1))
            if vertex in edges:
                for neighbor in edges[vertex]:
                    if neighbor in individual:
                        available_colors -= set([individual[neighbor]])

            if len(available_colors) > 0:
                color_fits = get_fitness_colors(individual, num_colors, edges, weights)
                placed = False
                for color in available_colors:
                    if weights[vertex] <= color_fits[color]:
                        individual[vertex] = color
                        placed = True
                        break
                if not placed:
                    individual[vertex] = random.choice(list(available_colors))
            else:
                individual[vertex] = num_colors + 1
                num_colors = num_colors + 1
        population.append(individual)
        used_colors.append(num_colors)
    return used_colors, population


def get_fitness_colors(individual, color_size, graph, weights):
    color_fitness = {}

    for color in range(1, color_size + 1):
        vertices = [v for v in range(len(graph)) if v in individual and individual[v] == color]
        # print("Vertices in color " + str(color) + " : ")
        # print(vertices)
        if not vertices:  # if color not used by any vertex
            color_fitness[color] = 0
        else:
            # Find the vertex with the maximum weight in this color
            max_weight_vertex = max(vertices, key=lambda v: weights[v])
            # print("Max weight vertex: " + str(max_weight_vertex) + " with weight " + str(weights[max_weight_vertex]) + "\n")
            # Check if this vertex is an endpoint (i.e., has an edge to only one other vertex)
            edges = [(u, v) for u in vertices for v in vertices if u != v and weights[v] != 0]
            if len(edges) == 1 and edges[0][0] == edges[0][1] == max_weight_vertex:
                # If the max weight vertex is an endpoint, assign 0 fitness to this color

                color_fitness[color] = 0
            else:
                # Otherwise, assign the weight of the max weight vertex to this color
                color_fitness[color] = weights[max_weight_vertex]
    return color_fitness


def train_combinations(combinations, min_solutions):
    for solution in min_solutions:
        solution_dict = {}
        for vertex in solution:
            if solution[vertex] not in solution_dict:
                solution_dict[solution[vertex]] = [vertex]
            else:
                solution_dict[solution[vertex]].append(vertex)
        for color in solution_dict:
            combinations.append(solution_dict[color])
    return combinations


def learn_frequent_items(combinations):
    freqs = {}
    for combination in range(len(combinations)):
        for i in range(len(combinations[combination]) - 1):
            for j in range(i + 1, len(combinations[combination])):
                if (combinations[combination][i], combinations[combination][j]) in freqs:
                    freqs[(combinations[combination][i], combinations[combination][j])] = freqs[(
                    combinations[combination][i], combinations[combination][j])] + 1
                elif (combinations[combination][j], combinations[combination][i]) in freqs:
                    freqs[(combinations[combination][j], combinations[combination][i])] = freqs[(
                    combinations[combination][j], combinations[combination][i])] + 1
                else:
                    freqs[(combinations[combination][i], combinations[combination][j])] = 1
    return freqs


def get_next_tuple(freqs, weights, degree):
    max_tuple = (0, 1)
    max_score = 0
    for freq in freqs:
        if (degree[freq[0]] + degree[freq[1]]) * freqs[freq] > max_score:
            max_score = (degree[freq[0]] + degree[freq[1]]) * freqs[freq]
            max_tuple = freq
        elif max_score != 0 and (degree[freq[0]] + degree[freq[1]]) * freqs[freq] == max_score and max(weights[freq[0]],
                                                                                                       weights[freq[
                                                                                                           1]]) > max(
                weights[max_tuple[0]], weights[max_tuple[1]]):
            max_tuple = freq
        else:
            continue
    del freqs[max_tuple]
    return max_tuple, freqs


def train_dataset(dataset_size, population_size, graph, vertex_size, weights):
    combinations = []
    for i in range(dataset_size):
        population = []
        used_colors, population = generate_initial_population(population_size, population, graph, vertex_size, weights)
        min_solutions = []
        min_score = 10000
        for i in range(len(used_colors)):
            if min_score > used_colors[i]:
                min_solutions.clear()
                min_solutions.append(population[i])
                min_score = used_colors[i]
            elif min_score == used_colors[i]:
                min_solutions.append(population[i])
            else:
                continue
        combinations = train_combinations(combinations, min_solutions)
    return combinations

def create_best_solution_from_learning(graph, weights, degree, frequents):
    color_size = 1
    solution = {}
    while len(frequents) > 0:
        each_tuple, frequents = get_next_tuple(frequents, weights, degree)
        if each_tuple[0] not in solution:
            for color in range(1, color_size + 1):
                conflict_free = True
                for neighbors in graph[each_tuple[0]]:
                    if neighbors in solution and solution[neighbors] == color:
                        conflict_free = False
                        break
                if conflict_free:
                    solution[each_tuple[0]] = color
                    break
            if each_tuple[0] not in solution:
                solution[each_tuple[0]] = color_size + 1
                color_size = color_size + 1
        if each_tuple[1] not in solution:
            conflict_free = True
            for neighbors in graph[each_tuple[1]]:
                if neighbors in solution and solution[neighbors] == solution[each_tuple[0]]:
                    conflict_free = False
                    break
            if conflict_free:
                solution[each_tuple[1]] = solution[each_tuple[0]]
                continue
            for color in range(1, color_size + 1):
                conflict_free = True
                for neighbors in graph[each_tuple[1]]:
                    if neighbors in solution and solution[neighbors] == color:
                        conflict_free = False
                        break
                if conflict_free:
                    solution[each_tuple[1]] = color
                    break
            if each_tuple[1] not in solution:
                solution[each_tuple[1]] = color_size + 1
                color_size = color_size + 1
    return solution, color_size

def validate_solution(graph, solution):
    for vertex in graph:
        for neighbor in graph[vertex]:
            if solution[neighbor] == solution[vertex]:
                return False
    return True

def write_solution_to_file(graph_name, solution_str):
    file1 = open("solutions.txt", "a")  # append mode
    file1.write(graph_name + ": " + solution_str + "\n")
    file1.close()

def print_solution(graph_name, solution, color_size, graph):
    solution_str = "||"
    for color in range(1, color_size+1):
        for vertex in graph:
            if solution[vertex] == color:
                solution_str += str(vertex) +", "
        solution_str += "||"
    write_solution_to_file(graph_name, solution_str)

def main(graph_name, population_size=50, dataset_size=25):
    runtime = 5
    avg_fitness = 0
    avg_time = 0
    min_used_colors = 1000
    best_solution = {}
    vertex_size, graph, degree = read_col_files("graphs/" + graph_name)
    # print(graph)
    weights = read_weights_file("graphs/" + graph_name + ".w", vertex_size)
    for run_id in range(runtime):
        # get the start time
        st = time.time()
        frequents = learn_frequent_items(train_dataset(dataset_size, population_size, graph, vertex_size, weights))
        solution, color_size = create_best_solution_from_learning(graph, weights, degree, frequents)
        # get the end time
        et = time.time()
        # get the execution time
        elapsed_time = et - st
        avg_fitness += color_size
        avg_time += elapsed_time
        if min_used_colors > color_size:
            min_used_colors = color_size
            best_solution = solution
        if validate_solution(graph, solution) is False:
            print("\nSolution is not correct\n")


    avg_time /= runtime
    avg_fitness /= runtime
    print(graph_name, '\t', avg_time, '\t', avg_fitness, '\t', min_used_colors)
    print_solution(graph_name, best_solution, min_used_colors, graph)


# Using readlines()
file1 = open('graphs', 'r')
Lines = file1.readlines()
print(Lines)

# Strips the newline character
for line in Lines:
    graphName = "{}".format(line.strip())
    main(graphName)
